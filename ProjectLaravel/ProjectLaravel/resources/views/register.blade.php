@extends('layouts.master')
@section('title')
Halaman Register
@endsection
@section('content')
    <h1>Buat Account Baru!</h1>
    <form action="welcome" method="post" >
        @csrf
        <h3>Sign Up Form</h3>
        <label>First Name:</label><br/>
        <input type="text" name="firstname"/><br/>
        <br/>
        <label>Last Name:</label><br/>
        <input type="text" name="lastname"/><br/>
        <br/>
        <label>Gender</label><br/>
        <input type="radio" name="gender" value="1"/> Male <br/>
        <input type="radio" name="gender" value="2"/> Female <br/>
        <input type="radio" name="gender" value="3"/> Other <br/>
        <br/>
        <label>Nationality:</label><br/>
        <br/>
        <select>
            <option value="indonesia">Indonesia</option>
            <option value="usa">USA</option>
            <option value="china">China</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapore">Singapore</option>
        </select><br/>
        <br/>
        <label >Language Spoken:</label><br/>
        <br/>
        <input type="checkbox" name="indonesia" value="1"/>Bahasa Indonesia <br/>
        <input type="checkbox" name="english" value="2" />English <br/>
        <input type="checkbox" name="other" value="3"/ >Other <br/>
        <br/>
        <label>Bio:</label><br/>
        <br/>
        <textarea id="bio" name="bio" rows="4" cols="50"/></textarea><br/>
    
        <input type="submit" name="submit" value="kirim"/>
    </form>
    <a href ="/">kembali</a>
@endsection