@extends('layouts.master')

@section('title')
Halaman Bio
@endsection

@section('content')
<div class="container">
    <h2>{{ $cast->nama }}</h2>
    <p>{{ $cast->bio }}</p>
</div>
<a href="/cast" class="btn btn-secondary">Back</a>
@endsection