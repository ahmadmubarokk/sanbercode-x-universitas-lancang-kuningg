<?php

require_once 'animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");

echo "nama : " . $sheep->get_name() . "<br>";
echo "legs : " . $sheep->get_legs() . "<br>";
echo "cold_blooded : " . $sheep->get_cold_blooded() . "<br><br>";

$sungokong = new Ape("kera sakti");

echo "nama : " . $sungokong->get_name() . "<br>";
echo "legs : " . $sungokong->get_legs() . "<br>";
echo "cold_blooded : " . $sungokong->get_cold_blooded() . "<br>";
echo "yell : ";
$sungokong->yell();
echo "<br><br>";

$kodok = new Frog("buduk");

echo "nama : " . $kodok->get_name() . "<br>";
echo "legs : " . $kodok->get_legs() . "<br>";
echo "cold_blooded : " . $kodok->get_cold_blooded() . "<br>";
echo "jump : ";
$kodok->jump();
?>
